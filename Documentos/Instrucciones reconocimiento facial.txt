OpenCV: Librería de visión artificial utilizada para el reconocimiento de movimientos, rostros y mucho más.
Reconocimiento facial.


Para instalar openCV se hace uso de PIP para descargar todas las dependencias. También se utiliza un entorno virtual con anaconda para tener todo lo necesario en dicho entorno.

Los comandos necesarios para instalar OpenCV deben ser ejecutados en el siguiente orden:
	pip install cmake
	pip install numpy scipy matplotlib scikit-learn jupyter
	pip install cmake
	pip install dlib

1. Se debe tener un dataSet de las personas que se quiera reconocer, entre más imágenes mejor.
	1.0 Para obtener las imágenes se puede hacer uso del proyecto registrarRostroDataset y así crear las imágenes.
	1.1 Se entra a la carpeta RetoAsistenteVirtual/reconocimientoFacial/dataset
	1.2 Para cada rostro se debe crear una carpeta. p.e /dataset/allan
	1.3 Las imágenes deben ser nombradas de 5 dígitos empezando en 00000, es decir, si son 10 imágenes, estas serían nombradas desde 00000 hasta 00001

2. Extraer los embeddings del dataset(extract_embeddings.py ): Se deben cargar las imágenes del dataSet para que sean reconocidas por el CV. para esto se extraen los embeddings de cada rostro y se guardan dentro del archivo mbeddings.pickle de la carpeta output como un archivo serializado. Además de extraer el serializado también se extrae el nombre de la carepta, el cuál será tomado como el nombre de la persona a la que le pertenece dicho rostro.
	**Comando:
python extract_embeddings.py --dataset dataset \
--embeddings output/embeddings.pickle \
--detector face_detection_model \
--embedding-model openface_nn4.small2.v1.t7

	**Parámetros
	--dataset : La ruta de donde se encuentra el dataSet con las imágenes.
	--embeddings : La ruta donde se guardarán las facciones encontradas que serán serializadas.
	--detector : Ruta donde se encuentra el detector de rostros utilizado en openCV. caffe-based
	--embedding-model : Rurta donde se encuentra el modelo openCV encargado de extraer el vector 128-D que diferencia los rostros.

3. Entrenar el reconocimiento facial(train_model.py): Se debe realizar el machinelearning para facilitar y aumentar la cobertura de reconocimiento de cada rostro. Se hace uso de la librería scikit-learn 
	**Comando:
python train_model.py --embeddings output/embeddings.pickle \
--recognizer output/recognizer.pickle \
--le output/le.pickle
	**Parámetros:


4. Reconocimiento facial por imágenes.
	Si se quiere validar el reconocimiento a través de una imagen, estas deben ser puestas en la carpeta /images.
	**Comando:
python recognize.py --detector face_detection_model \
--embedding-model openface_nn4.small2.v1.t7 \
--recognizer output/recognizer.pickle \
--le output/le.pickle \
--image images/Todos.jpg

5. Reconocimiento facial en streaming.
python recognize_video.py --detector face_detection_model \
--embedding-model openface_nn4.small2.v1.t7 \
--recognizer output/recognizer.pickle \
--le output/le.pickle

Construir dataset personalizado
En el proyecto registrarRostroDataset se puede crear una serie de imágenes que serán utilizadas como un dataSet.
Con la tecla K se toma la foto.
COn la tecla q se sale del aplicativo.

	**Comando
python build_face_dataset.py --cascade haarcascade_frontalface_default.xml \
--output dataset/<carpeta>

	**Parámetros
	--cascade : Ruta donde se especifica el modelo de reconocimiento a utilizar. En este caso es Haar.Este es el rectángulo que aparece en los rostros que se detecta.
	--output: Es la carpeta donde se guardarán las imágenes.



Extraer embbedings y realizar entrenamiento.
python extract_embeddings.py --dataset dataset \
--embeddings output/embeddings.pickle \
--detector face_detection_model \
--embedding-model openface_nn4.small2.v1.t7 && python train_model.py --embeddings output/embeddings.pickle \
--recognizer output/recognizer.pickle \
--le output/le.pickle



Comandos útiles
Se está utilizando entornos virtuales en el que cada uno tiene instaladas las utilidades y librerías a utilizar.
workon cv.
Para ejecutar el chatBot es necesario instalar las siguientes librerías.
	pip3 install ChatterBot
	pip install git+https://github.com/gunthercox/chatterbot-corpus.git#egg=chatterbot-corpus
	python3 chat.py

Comando para instalar la GUI de python. tkinter
	sudo apt-get install python3-tk


